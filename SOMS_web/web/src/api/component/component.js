import request from '@/utils/request'


export default{
    // 零件种类查询分页
    // current当前页
    // size每页记录数
    getComponentList(current,size){
        return request({
            url: `/tbl-component/queryAll/${current}/${size}`,
            method: 'post'
          })     
    },
    putOrder(list){
        return request({
            url: `/tbl-order/recommendOrder`,
            method: 'post',
            data: 
                list
          }) 
    }
}
