package com.soms.server.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
/***
 * @author Guowei
 * @date 2020/6/11 16:03
 * @version 1.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 表单认证
        http.formLogin()
                //当发现/login时认为是登录，需要执行UserDetailsServiceImpl
                .loginProcessingUrl("/login")
                .loginPage("/login.html");

        // url 拦截 (授权)
        http.authorizeRequests()
                //关闭拦截
                .anyRequest().permitAll();


        //关闭csrf防护
        http.csrf().disable();


    }

    @Bean
    public PasswordEncoder getPe(){
        return new BCryptPasswordEncoder();
    }
}
