package com.soms.server.mapper;

import com.soms.server.entity.TblOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 订购商表 Mapper 接口
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Repository
public interface TblOrderMapper extends BaseMapper<TblOrder> {

}
