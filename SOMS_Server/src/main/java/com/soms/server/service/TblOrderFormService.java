package com.soms.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.soms.server.entity.TblOrderForm;

import java.util.Map;

/**
 * <p>
 * 订单表是订购商下单的表，内容以列存储的形式进行存储，同一订单编号视为同一订单里的内容。 服务类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-15
 */
public interface TblOrderFormService extends IService<TblOrderForm> {
    /**
     * 订购零件推荐
     * @param typeandnum 订购商要订购的零件种类和对应的数量
     * @return 对应生产商生产的零件id
     */
    public Integer[] compoentRecomm(Map<Integer,Integer> typeandnum) throws Exception;
}
