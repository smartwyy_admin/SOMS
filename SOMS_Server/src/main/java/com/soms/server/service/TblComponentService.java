package com.soms.server.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblComponent;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 零件表 服务类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */

public interface TblComponentService extends IService<TblComponent> {
    /**
     *<p>
     *     查询所有零件
     *</p>
     * @return 零件
     */
    public List<TblComponent> queryAll();

    /**
     * <p>
     *     分页查询零件的种类
     * </p>
     * @param page  包含了分页相关的数据，像当前页，总页数，总记录等
     * @return page 返回的和输入的是同一个对象，详细请参考mybatis-plus分页机制
     */
    public Page<TblComponent> queryPage(Page page);


    /**
     * <p>
     *     根据名字查询
     * </p>
     * @param name 零件名字
     * @return 零件数据
     */
    public List<TblComponent> queryByname(String name);

    /**
     * <p>
     *     添加的零件
     * </p>
     * @param component 添加零件
     * @return 添加的数据的条数 ，默认为1
     */
    public int addByname(TblComponent component) throws Exception;
}
