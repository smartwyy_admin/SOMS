package com.soms.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblLinkman;
import com.soms.server.entity.vo.LinkmanVo;
import com.soms.server.mapper.TblLinkmanMapper;
import com.soms.server.service.TblLinkmanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 供应商联系人表 服务实现类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-23
 */
@Service
public class TblLinkmanServiceImpl extends ServiceImpl<TblLinkmanMapper, TblLinkman> implements TblLinkmanService {

    @Override
    public Page<LinkmanVo> pageLinkman(Page page) {
        QueryWrapper<LinkmanVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.orderByAsc("supplier_id");
        return this.baseMapper.queryAllLinkman(page);
    }

    @Override
    public LinkmanVo queryLinkmanById(Integer id) {
        return this.baseMapper.queryLinkmanById(id);
    }
}
