package com.soms.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.entity.TblComponent;
import com.soms.server.entity.TblSupplierComponent;
import com.soms.server.entity.vo.SupplierComponentVo;
import com.soms.server.mapper.TblComponentMapper;
import com.soms.server.mapper.TblSupplierComponentMapper;
import com.soms.server.service.TblSupplierComponentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 供应商零件关系表 服务实现类
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-11
 */
@Service
public class TblSupplierComponentServiceImpl extends ServiceImpl<TblSupplierComponentMapper, TblSupplierComponent> implements TblSupplierComponentService {



    @Autowired
    private TblComponentMapper tblComponentMapper;
    @Override
    public Page<TblSupplierComponent> queryAllForPage(Page page) {
        QueryWrapper<TblSupplierComponent> queryWrapper = new QueryWrapper<>();
        //queryWrapper.eq("id",9);
        //queryWrapper.eq("onsupply", "正在供应")
        Page selectPage = this.baseMapper.selectPage(page, queryWrapper);
        return selectPage;
    }

    @Override
    public int addTblCompoent(TblSupplierComponent tblSupplierComponent) throws Exception {
        QueryWrapper<TblComponent> componentQueryWrapper = new QueryWrapper<>();
        componentQueryWrapper.eq("id", tblSupplierComponent.getComponentId());
        List<TblComponent> tblComponents = tblComponentMapper.selectList(componentQueryWrapper);
        if(tblComponents.isEmpty()){
            throw new Exception("该零件种类不存在，请新建零件种类");
        }else {
            int insert = this.baseMapper.insert(tblSupplierComponent);
            return insert;
        }

    }

    @Override
    public Page<SupplierComponentVo> pageSupplierComponent(Page page) {
        QueryWrapper<SupplierComponentVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sc.is_deleted",0);
        Page<SupplierComponentVo> page1 = this.baseMapper.queryAllSupplierComponent(page,queryWrapper);
        return page1;
    }

    @Override
    public int deletesupplierComponentById(Integer id) {
        return this.baseMapper.deleteById(id);
    }

    @Override
    public List<SupplierComponentVo> querySupplierComponentByComponentId(int componentId) {
        QueryWrapper<SupplierComponentVo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sc.is_deleted",0);
        queryWrapper.eq("sc.component_id",componentId);
        List<SupplierComponentVo > list = this.baseMapper.queryAllSupplierComponent(new Page<>(),queryWrapper).getRecords();
        return list;
    }

    @Override
    public SupplierComponentVo querySupplierCompentById(Integer id) {
        SupplierComponentVo supplierComponentVo = this.baseMapper.querySupplierCompentById(id);
        return supplierComponentVo;
    }

    @Override
    public int updateSupplierComponentById(SupplierComponentVo supplierComponentVo) {
        TblSupplierComponent supplierComponent = new TblSupplierComponent();
        supplierComponent
                .setId(supplierComponentVo.getId())
                .setComponentId(supplierComponentVo.getComponentId())
                .setSupplierId(supplierComponentVo.getSupplierId())
                .setPrice(supplierComponentVo.getPrice());
        int count = this.baseMapper.updateById(supplierComponent);
        return count;
    }
}
