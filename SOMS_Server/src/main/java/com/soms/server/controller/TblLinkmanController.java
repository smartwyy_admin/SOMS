package com.soms.server.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.soms.server.common.JsonResult;
import com.soms.server.entity.TblLinkman;
import com.soms.server.entity.TblSupplier;
import com.soms.server.entity.vo.LinkmanVo;
import com.soms.server.mapper.TblLinkmanMapper;
import com.soms.server.mapper.TblSupplierMapper;
import com.soms.server.service.TblLinkmanService;
import com.soms.server.service.TblSupplierService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLOutput;
import java.util.List;

/**
 * <p>
 * 供应商联系人表 前端控制器
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-23
 */
@RestController
@RequestMapping("/tbl-linkman")
@CrossOrigin
public class TblLinkmanController {
    @Autowired
    TblLinkmanService linkmanService;
    @Autowired
    TblSupplierService supplierService;
    @Autowired
    TblLinkmanMapper linkmanMapper;
    @ApiOperation("分页查询供应商联系人")
    @GetMapping("pageLinkman/{current}/{size}")
    public JsonResult pageLinkman(@ApiParam(name = "current", value = "当前页", required = true)
                                      @PathVariable Integer current,
                                  @ApiParam(name = "size", value = "每页结果数", required = true)
                                      @PathVariable Integer size){
        Page page = new Page(current,size);
        Page<LinkmanVo> page1 = linkmanService.pageLinkman(page);
        return new JsonResult(true,page1);
    }

    @ApiOperation("根据id删除供应商联系人")
    @DeleteMapping("{id}")
    public JsonResult deleteLinkman(@ApiParam(name = "id", value = "id", required = true)
                                    @PathVariable Integer id){

        int count = linkmanService.getBaseMapper().deleteById(id);
        return new JsonResult(true,count);
        }
    @ApiOperation("根据id查询供应商联系人信息")
    @GetMapping("queryLinkmanById/{id}")
    public JsonResult queryLinkmanById(@ApiParam(name = "id", value = "id", required = true)
                                    @PathVariable Integer id){

        LinkmanVo linkmanVo = linkmanService.queryLinkmanById(id);
        return new JsonResult(true,linkmanVo);
        }
    @ApiOperation("查找所有的供应商信息")
    @GetMapping("getSupplierOptions")
    public JsonResult getSupplierOptions(){

        List<TblSupplier> list = supplierService.getBaseMapper().selectList(null);

        return new JsonResult(true,list);
        }
    @ApiOperation("修改联系人信息")
    @PostMapping("updateLinkman")
    public JsonResult updateLinkman(@RequestBody TblLinkman linkman){


        int count = linkmanService.getBaseMapper().updateById(linkman);
        return new JsonResult(true,count);
        }
    @ApiOperation("添加联系人")
    @PostMapping("addLinkman")
    public JsonResult addLinkman(@RequestBody TblLinkman linkman){


        int count = linkmanService.getBaseMapper().insert(linkman);
        return new JsonResult(true,count);
        }

}
