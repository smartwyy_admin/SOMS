package com.soms.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 订单表是订购商下单的表，内容以列存储的形式进行存储，同一订单编号视为同一订单里的内容。
 * </p>
 *
 * @author generator_gw
 * @since 2020-06-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("Tbl_order_form")
@ApiModel(value="TblOrderForm对象", description="订单表是订购商下单的表，内容以列存储的形式进行存储，同一订单编号视为同一订单里的内容。")
public class TblOrderForm implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "默认id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单id")
    private String orderFormId;

    @ApiModelProperty(value = "订购商零件表id")
    private Integer componentId;

    @ApiModelProperty(value = "订购数量")
    private Integer number;

    @ApiModelProperty(value = "订购商id")
    private Integer orderId;

    @ApiModelProperty(value = "下单日期")
    private Date orderDate;

    @ApiModelProperty(value = "更新订单内容的日期")
    private Date upDate;

    @ApiModelProperty(value = "订单状态")
    private String state;


}
